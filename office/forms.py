from django import forms
from .models import Petition, Quote, CreatedQuote, Pqrs

class PetitionForm(forms.ModelForm):
    class Meta:
        model = Petition
        fields = ['hc_file', 'or_file', 'comments']

    def clean(self):
        cleaned_data = super().clean()
        hc_file = cleaned_data.get('hc_file')
        or_file = cleaned_data.get('or_file')

        if not hc_file and not or_file:
            raise forms.ValidationError('Debes adjuntar al menos un archivo.')

        return cleaned_data


class QuoteForm(forms.ModelForm):
    class Meta:
        model = Quote
        fields = ['ips', 'doctor', 'date', 'hour', 'instructions']

class CreatedQuoteForm(forms.ModelForm):
    class Meta:
        model = CreatedQuote
        fields = ['especialidad', 'ips', 'doctor', 'date', 'hour']


class PqrsForm(forms.ModelForm):
    class Meta:
        model = Pqrs
        fields = ['first_name', 'card_type', 'card_number', 'gender', 'birth_date', 'cell_phone', 'phone', 'email']