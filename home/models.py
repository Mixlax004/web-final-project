from django.db import models
from django.contrib.auth.models import AbstractUser, Group, Permission
from django.core.validators import MaxLengthValidator, RegexValidator
from datetime import date
from django.contrib.auth.models import User
from choices import *

# Create your models here.
   
"""class User(models.Model):
    first_name = models.CharField('Nombres', max_length=50, null=False)
    last_name = models.CharField('Apellidos', max_length=50, null=False)
    card_type = models.CharField('Tipo de documento', choices=CARD_TYPE, max_length=22, null=False, blank=False)
    card_number = models.CharField('Número de documento', max_length=10, default='0', null=False, validators=[RegexValidator(regex='^[0-9]{10}$')])
    regime = models.CharField('Regimen', choices=REGIME, max_length=13,null=False)
    gender = models.CharField('Género', choices=GENDER, max_length=9, null=False)
    birth_date = models.DateField('Fecha de nacimiento', default=date(2000, 1, 1), null=False)
    zone = models.CharField('Zona', choices=ZONE, max_length=30, null=False, blank=False)
    adress = models.CharField('Dirección de residencia', max_length=30, null=False, blank=False)
    cell_phone = models.CharField('Celular', max_length=10, null=False, blank=False, validators=[RegexValidator(regex='^[0-9]{10}$')])
    phone = models.CharField('Teléfono', max_length=10, null=False, blank=True, validators=[RegexValidator(regex='^[0-9]{10}$')])
    work_adress = models.CharField('Dirección de trabajo', max_length=30, null=True, blank=True)
    work_phone = models.CharField('Teléfono de trabajo', max_length=10, null=False, blank=True, validators=[RegexValidator(regex='^[0-9]{10}$')])

    email = models.EmailField('Correo electrónico', unique=True, null=False, blank=False)
    is_registered = models.BooleanField(default=False)
    username = None
    groups = None
    user_permissions = None

    def __str__(self):
        return self.card_number
    
    
    
    REQUIRED_FIELDS = ['first_name', 'last_name', 'card_type', 'card_number', 'regime', 'gender', 'birth_date', 'zone', 'adress', 'cell_phone', 'email']
"""
