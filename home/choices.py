CARD_TYPE = (
    ('cedula', 'Cédula de Ciudadanía'),
    ('tarjeta', 'Tarjeta de Identidad'),
    ('extranjeria', 'Cédula de Extranjería'),
    ('pasaporte', 'Pasaporte'),
    ('reg_civil', 'Registro Civil'),
    ('adul_no_identificado', 'Adulto no identificado'),
    ('menor_no_identificado', 'Menor no identificado'),
    ('diplomatico', 'Documento Diplomático'),
    ('salvo_conducto', 'Salvoconducto'),
    ('pep', 'Permiso especial de permanencia'),
    ('ppt', 'Permiso por proteccion temporal')
)

REGIME = (
    ('subsidiado', 'Susidiado'),
    ('contributivo', 'Contributivo'),
    ('beneficiario', 'Beneficiario'),
)

GENDER = (
    ('femenino', 'Femenino'),
    ('Masculino', 'Masculino'),
)

ZONE = (
    ('rural', 'Rural'),
    ('Urbana', 'Urbana'),
)