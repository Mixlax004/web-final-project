import random
from django.db import models
import uuid
from datetime import datetime
from django.core.validators import FileExtensionValidator
from django.contrib.auth.models import User
from django.core.validators import MaxLengthValidator, RegexValidator
from datetime import date
from choices import *

class Petition(models.Model):

    number = models.AutoField(primary_key=True, default=uuid.uuid4, editable=False)
    date = models.DateField(auto_now=True)
    status = models.CharField(max_length=100)
    comments = models.TextField()  
    response = models.TextField()
    hc_file = models.FileField(upload_to='attached_files/', null=True, blank=False)
    or_file = models.FileField(upload_to='attached_files/', null=True, blank=False)
    def save(self, *args, **kwargs):
        
        if not self.request_number:
            self.request_number = random.randint(100000, 999999)
        super().save(*args, **kwargs)

    def __str__(self):
        return f"Petition #{self.number}"
    
class Authorization(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=255)
    ips = models.CharField(max_length=255)
    b_date = models.DateField()
    e_date = models.DateField()
    auth_file = models.FileField(upload_to='authorization_file/')

    auth_file = models.FileField(
        upload_to='authorization_file/',
        validators=[FileExtensionValidator(allowed_extensions=['pdf'])]
    )

    def __str__(self):
        return f'Autorización de {self.name} para {self.ips} ({self.b_date} a {self.e_date})'

class Quote(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    ips = models.CharField(max_length=255)
    doctor = models.CharField(max_length=255)
    date = models.DateField()
    hour = models.TimeField()
    instructions = models.BooleanField(default=False, help_text='Marcar para no permitir nuevas citas')
    available = models.BooleanField(default=True, help_text='Marcar para indicar que la cita está disponible')

    def __str__(self):
        return f"Cita para {self.user.user} con el Dr. {self.doctor} el {self.date} a las {self.hour}"

class AvailableQuote(models.Model):
    ips = models.CharField(max_length=255)
    doctor = models.CharField(max_length=255)
    date = models.DateField()
    hour = models.TimeField()
    instructions = models.BooleanField(default=False, help_text='Marcar para no permitir nuevas citas')
    available = models.BooleanField(default=True, help_text='Marcar para indicar que la cita está disponible')

    def __str__(self):
        return f"Cita disponible con el Dr. {self.doctor} el {self.date} a las {self.hour}"

class CreatedQuote(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    especialidad = models.CharField(max_length=255)
    ips = models.CharField(max_length=255)
    doctor = models.CharField(max_length=255)
    date = models.DateField()
    hour = models.TimeField()
    instructions = models.BooleanField(default=False, help_text='Marcar para no permitir nuevas citas')
    available = models.BooleanField(default=True, help_text='Marcar para indicar que la cita está disponible')

    def __str__(self):
        return f"Cita pendiente con el Dr. {self.doctor} el {self.date} a las {self.hour}"
    


class Pqrs(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    first_name = models.CharField('Nombre completo', max_length=50, null=False)
    card_type = models.CharField('Tipo de documento', choices=CARD_TYPE, max_length=22, null=False, blank=False)
    card_number = models.CharField('Número de documento', max_length=10, default='0', null=False, validators=[RegexValidator(regex='^[0-9]{10}$')])
    gender = models.CharField('Género', choices=GENDER, max_length=9, null=False)
    birth_date = models.DateField('Fecha de nacimiento', default=date(2000, 1, 1), null=False)
    
    cell_phone = models.CharField('Celular', max_length=10, null=False, blank=False, validators=[RegexValidator(regex='^[0-9]{10}$')])
    phone = models.CharField('Teléfono', max_length=10, null=False, blank=True, validators=[RegexValidator(regex='^[0-9]{10}$')])
    email = models.EmailField('Correo electrónico', unique=True, null=False, blank=False)
    
    def __str__(self):
        return self.card_number
    
    
    
    REQUIRED_FIELDS = ['first_name',  'card_type', 'card_number',  'gender', 'birth_date', 'cell_phone', 'email']