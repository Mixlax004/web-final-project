from django.urls import path
from . import views

app_name = 'office' 

urlpatterns = [
    path('p/    ', views.profile, name='profile'),
    path('a/', views.authorizations, name='authorizations'),
    path('ya/', views.authorized, name='authorized'),
    path('na/', views.new_auth, name='new_auth'),
    path('c/', views.certifications, name='certifications'),
    path('mq/', views.module_quotes, name='module_quotes'),
    path('q/', views.quotes, name='quotes'),
    path('aq/', views.available_quotes, name='available_quotes'),
    path('cq/', views.created_quotes, name='created_quotes'),
    path('pe/', views.personal, name='personal'),
    path('logout/', views.log_out, name='logout'),
    path('pqr/', views.pqr, name='pqr')
]