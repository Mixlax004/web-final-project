from django.shortcuts import render, redirect
from .forms import *
from .models import *
from datetime import datetime
import uuid
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, logout
from django.contrib import messages
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from .models import CreatedQuote 
from datetime import datetime

@login_required
def profile(request):
    return render(request, 'profile.html')

@login_required
def authorizations(request):
    return render(request, 'authorizations.html')

@login_required
def certifications(request):
    return render(request, 'certifications.html')
@login_required
def created_quotes(request):
    if request.method == 'POST':
        especialidad = 'Medicina General'
        doctor = request.POST.get('doctor')
        date_str = request.POST.get('date')
        hour_str = request.POST.get('hour')
        ips = request.POST.get('ips')

        print("Datos del formulario:")
        print(f"Doctor: {doctor}, Fecha: {date_str}, Hora: {hour_str}, IPS: {ips}")

        # Convertir la cadena de fecha al formato correcto
        date = datetime.strptime(date_str, '%b. %d, %Y').strftime('%Y-%m-%d')
        try:
            hour = datetime.strptime(hour_str, '%I:%M %p').strftime('%H:%M')
        except ValueError:
            # Manejar el caso en que la cadena de hora no tiene el formato esperado
            # Puedes agregar lógica adicional aquí según tus requisitos
            hour = None

        # Verificar si la hora se pudo convertir correctamente
        if hour is not None:
            # Crea la cita agendada en la nueva tabla
            created_quote = CreatedQuote.objects.create(
                especialidad=especialidad,
                ips=ips,
                doctor=doctor,
                date=date,
                hour=hour,
                
            )
            created_quote.save()

            print("Cita creada:", created_quote)

        created_quotes = CreatedQuote.objects.all()
        
    print("Citas agendadas:", created_quotes)

    available_quotes = []  # Asegúrate de obtener las citas disponibles de alguna manera

    return render(request, 'quotes.html', {'created_quotes': created_quotes, 'available_quotes': available_quotes})

@login_required
def available_quotes(request):
    created_quotes = CreatedQuote.objects.all().filter(available=True)
    available_quotes = AvailableQuote.objects.filter(available=True)
    return render(request, 'quotes.html', {'created_quotes': created_quotes,'available_quotes': available_quotes})

def module_quotes(request):
    created_quotes = CreatedQuote.objects.all()
    return render(request, 'quotes.html', {'created_quotes': created_quotes})

def quotes(request):
    if request.method == 'POST':
        form = QuoteForm(request.POST)
        if form.is_valid():
            quote = form.save(commit=False)
            quote.user = request.user  # Asigna el usuario actual a la cita
            quote.save()
            messages.success(request, 'Cita creada exitosamente.')
            return redirect('quotes')  # Redirige a la misma página después de crear la cita
        else:
            messages.error(request, 'Error en el formulario. Por favor, corrige los errores.')
    else:
        form = QuoteForm()

    return render(request, 'quotes.html', {'form': form})

@login_required
def personal(request):
    return render(request, 'personal.html')

@login_required
def authorized(request):
    
    user_authorizations = Authorization.objects.all()

    context = {
        'authorization': user_authorizations,
    }

    return render(request, 'authorized.html', context)

@login_required
def new_auth(request):
    if request.method == 'POST':
        form = PetitionForm(request.POST, request.FILES)

        if form.is_valid():
            petition = form.save(commit=False)
            petition.id = uuid.uuid4()  
            petition.date = datetime.now()
            petition.save()
            return redirect('new_auth')  
    else:
        form = PetitionForm()

    return render(request, 'new_auth.html', {'form': form})

@login_required
def log_out(request):
    logout(request)
    return redirect('index')

def pqr(request):
    if request.method == 'POST':
        form = PqrsForm(request.POST)
        if form.is_valid():
            form.save()
            # Agrega lógica adicional según tus necesidades
            return redirect('gracias')
    else:
        form = PqrsForm()

    return render(request, 'pqr.html', {'form': form})