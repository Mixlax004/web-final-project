from django.contrib import admin
from .models import *

admin.site.register(Petition)
admin.site.register(Quote)
admin.site.register(Authorization)
admin.site.register(AvailableQuote)
admin.site.register(CreatedQuote)
admin.site.register(Pqrs)