from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib import messages
from django.http import HttpResponse
from django.contrib.auth.forms import AuthenticationForm

def register(request): 
    #error_message = None
    if request.method == 'POST':
        email = request.POST.get('email')
        first_name = request.POST.get('first_name')
        uname = request.POST.get('username')
        pswrd1 = request.POST.get('password1')
        pswrd2 = request.POST.get('password2')
        if User.objects.filter(username=uname).exists():
            return render(request, 'login.html', {'error': 'El usuario ya existe'})
        if pswrd1 != pswrd2:
            return render(request, 'register.html', {'error': 'Contraseñas no coinciden'})
        else:
            user = User.objects.create_user(uname,email,pswrd1)
            user.save()
            login(request, user)
            return redirect('login')

    return render(request, 'register.html')


def log_in(request):
    error = None
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        print(f'Intento de inicio de sesión con usuario: {username}')
        
        user = authenticate(request, username=username, password=password)
        
        print(f'Usuario autenticado: {user}')
        user = authenticate(request,username=username,password=password)
        if user is not None:
            login(request,user)
            return redirect('office:profile')
        else:
            return render(request, 'login.html', {'error': 'Nombre de usuario o contraseña incorrectos'})
    return render(request, 'login.html')



"""def register(request): 
    error_message = None
    form = RegisterForm(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            pswrd1 = request.POST.get('password1')
            pswrd2 = request.POST.get('password2')
            if pswrd1 != pswrd2:
                return HttpResponse('Las contraseñas no coinciden ')
            else:
                user = form.save()
                login(request,user)
                messages.success(request, "Registro Exitoso")
            return redirect('login', id=user.id) 
    return render(request, 'register.html', {'error_message': error_message})

    
"""
"""def register(request):
    return render(request, 'register.html')

def register_eps_user(request):
    form = RegisterForm(request.POST or None)  # Asegura que el formulario se inicializa correctamente

    if request.method == 'POST':
        if form.is_valid():
            card_type = form.cleaned_data['card_type']
            card_number = form.cleaned_data['card_number']
            try:
                eps_user = EpsUser.objects.get(card_type=card_type, card_number=card_number)
            except EpsUser.DoesNotExist:
                eps_user = None

            if eps_user is not None:
                if not eps_user.is_registered:
                    form.instance.set_unusable_password()

                user = form.save(commit=False)
                user.save()
                return redirect('update_info', pk=eps_user.pk)
            else:
                form.add_error(None, 'Usuario no encontrado')
        # Pendiente mensaje de error de por qué no se puede registrar

    return render(request, 'register.html', {'form': form})
@login_required
def update_info_user(request, pk):
    try:
        user = EpsUser.objects.get(pk=pk)
    except EpsUser.DoesNotExist:
        return HttpResponse(status=404)

    form = UpdateForm(request.POST or None, instance=user)

    if request.method == 'POST':
        if form.is_valid():
            form.save(commit=True)
            return redirect('profile')

    return render(request, 'update_info.html', {'form': form})

def user_login(request):
    form = AuthenticationForm(request, request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            # Obtener el pk del usuario autenticado
            user_pk = request.user.pk  # Corrección aquí
            return redirect('office:profile', pk=user_pk)
        else:
            messages.error(request, 'Credenciales inválidas. Inténtalo de nuevo.')

    return render(request, 'login.html', {'form': form})


@login_required
def user_logout(request):
    logout(request)
    return redirect('index.html')"""
