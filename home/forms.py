from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

CARD_TYPE = (
    ('cedula', 'Cédula de Ciudadanía'),
    ('tarjeta', 'Tarjeta de Identidad'),
    ('extranjeria', 'Cédula de Extranjería'),
    ('pasaporte', 'Pasaporte'),
    ('reg_civil', 'Registro Civil'),
    ('adul_no_identificado', 'Adulto no identificado'),
    ('menor_no_identificado', 'Menor no identificado'),
    ('diplomatico', 'Documento Diplomático'),
    ('salvo_conducto', 'Salvoconducto'),
    ('pep', 'Permiso especial de permanencia'),
    ('ppt', 'Permiso por proteccion temporal')
)

class RegisterForm(UserCreationForm):
    email = forms.EmailField(label='Correo:', widget=forms.TextInput, required=True)
    first_name = forms.CharField(label='Nombre completo:', widget=forms.TextInput, required=True)
    username = forms.CharField(label='Número de documento:', widget=forms.TextInput, required=True)
    password1 = forms.CharField(label='Contraseña:', widget=forms.PasswordInput, required=True)
    password2 = forms.CharField(label='Confirma contraseña:', widget=forms.PasswordInput, required=True)

    class Meta:
        model = User
        fields = ['email', 'first_name', 'username', 'password1', 'password2']
        help_text = {k: "" for k in fields}

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.username = self.cleaned_data['username']  
        user.first_name = self.cleaned_data['first_name']
        if commit:
            user.save()
        return user

"""class RegisterForm(forms.ModelForm):
        card_type = forms.ChoiceField(choices=CARD_TYPE,  label='Tipo de documento' )
        card_number = forms.CharField(label='Numero de documento', widget=forms.TextInput(attrs={'type': 'number'}))
        first_name = forms.CharField(label='Nombres')
        last_name = forms.CharField(label='Apellidos')
        email = forms.CharField(label='correo', widget=forms.TextInput(attrs={'type': 'email0'}))
        class Meta:
            model = EpsUser
            fields = ['card_type', 'card_number', 'first_name', 'last_name', 'email']
            
        def save(self, commit=True):
            user = super(RegisterForm, self).save(commit=False)

        # Asignación de valores de los campos del formulario al usuario
            user.email = self.cleaned_data['email']
            user.first_name = self.cleaned_data['first_name']
            user.last_name = self.cleaned_data['last_name']
            user.card_type = self.cleaned_data['card_type']
            user.card_number = self.cleaned_data['card_number']
            user.regime = self.cleaned_data['regime']
            self.fields['card_number'].widget.attrs['maxlength'] = 10

            if commit:
                user.save()	
            return user

 
class UpdateForm(forms.ModelForm):
   
        email = forms.CharField(label='Correo electrónico')
        zone = forms.ChoiceField(choices=ZONE, label='Zona')
        adress = forms.CharField(label='Dirección de residencia')
        cell_phone = forms.CharField(label='Celular')
        phone = forms.CharField(label='Telefono')
        work_adress = forms.CharField(label='Dirección de trabajo')
        work_phone = forms.CharField(label='Telefono de trabajo')

        class Meta:
            model = EpsUser
            fields = ['zone', 'adress', 'cell_phone', 'phone', 'work_adress', 'work_phone', 'email']"""
