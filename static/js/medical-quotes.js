let citas = [
    {
        "specialty":"Psiquiatría",
        "medic":"Richard Stallman",
        "date":"2023-10-01",
        "hour":"10:00",
        "status":"Cancelado"
    },
    {
        "specialty":"Urología",
        "medic":"Edward Snowden",
        "date":"2023-09-23",
        "hour":"9:40",
        "status":"Por completar"
    },
    {
        "specialty":"Dermatología",
        "medic":"Linus Torvalds",
        "date":"2023-09-18",
        "hour":"14:00",
        "status":"Por completar"
    },
    {
        "specialty":"Otorrinolaringología",
        "medic":"Aaron Swartz",
        "date":"2023-08-15",
        "hour":"15:30",
        "status":"Completado"
    }
]

let table = document.getElementById("citas-table");

function addData(citas){
    var results_amount = citas.length;

    i=0;
    if (Object.keys(citas).length != 0){
        while (i < 10 && i < citas.length) {
            var row = table.insertRow(-1);
            var specialty = row.insertCell(0);
            var medic = row.insertCell(1);
            var date = row.insertCell(2);
            var hour = row.insertCell(3);
            var stat = row.insertCell(4);
            
            specialty.innerText = citas[i].specialty;
            medic.innerText = citas[i].medic;
            date.innerText = citas[i].date;
            hour.innerText = citas[i].hour;
            stat.innerText = citas[i].status;
            i++;
          }
    }

}
